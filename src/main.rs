use clap::StructOpt;
use colored::*;
use regex::{Match, Regex};
use std::collections::HashSet;
use std::fs::rename;
use std::io::Write;
use std::path::{Path, PathBuf};

#[derive(StructOpt, Debug)]
#[structopt(name = "Show renaming")]
struct Opt {
    #[structopt(short, long)]
    season: Option<i16>,
    #[structopt(short, long)]
    name: Option<String>,
    #[structopt(default_value = "0", short, long)]
    offset: i16,
    #[structopt(short, long)]
    keep_square_brackets: bool,
    #[structopt(short, long)]
    yes_to_rename_prompt: bool,
    #[structopt(parse(from_os_str))]
    files: Vec<PathBuf>,
}

struct ToRename<'a> {
    original: &'a Path,
    new: String,
}

fn main() {
    let opt: Opt = clap::Parser::parse();
    let mut rename_list: Vec<ToRename> = Vec::new();
    for item in opt.files.iter() {
        if !item.exists() {
            println!(
                "{}",
                format!("file does not exist: {}", item.to_string_lossy()).red()
            );
            return;
        }
        let rename = gen_rename(item, &opt);
        if let Some(rename) = rename {
            rename_list.push(rename);
        }
    }
    let to_print = to_print(&rename_list);
    if let Some(to_print) = to_print {
        print!("{}", to_print);
    }
    let mut set = HashSet::new();
    for i in rename_list.iter() {
        let res = set.insert(&i.new);
        if !res {
            println!("{}", format!("Duplicate name found: {}", i.new).red());
            return;
        }
    }
    if opt.yes_to_rename_prompt {
        save_changes(rename_list);
    } else {
        ask_to_confirm_changes(rename_list)
    }
}

fn to_print(rename_list: &Vec<ToRename>) -> Option<String> {
    let mut longest_name = 0;
    rename_list.iter().for_each(|item| {
        let len = item.original.to_string_lossy().len();
        if len > longest_name {
            longest_name = len;
        }
    });
    let to_print = rename_list
        .iter()
        .map(|i| {
            format!(
                "{:<width$} -> {}\n",
                i.original.to_string_lossy(),
                i.new,
                width = longest_name
            )
        })
        .reduce(|mut accum, item| {
            accum.push_str(&item);
            accum
        });
    to_print
}

fn ask_to_confirm_changes(rename_list: Vec<ToRename>) {
    loop {
        print!("Do you whish to make these changes(y/n) ");
        let _res = std::io::stdout().flush();
        let mut input = String::new();
        std::io::stdin().read_line(&mut input).unwrap();
        if input.trim().to_lowercase() == "y" {
            save_changes(rename_list);
            return;
        } else if input.trim().to_lowercase() == "n" {
            return;
        }
    }
}

fn save_changes(rename_list: Vec<ToRename>) {
    for change in rename_list {
        let res = rename(change.original, &change.new);
        if let Err(e) = res {
            println!(
                "{}",
                format!(
                    "Error encountered while renaming {} to {}\n Error: {}",
                    change.original.to_str().unwrap(),
                    &change.new,
                    e
                )
                .red()
            )
        }
    }
}

fn gen_rename<'a>(item: &'a PathBuf, opt: &Opt) -> Option<ToRename<'a>> {
    let re_digits = Regex::new(r"\d+").unwrap();
    let re_data = Regex::new(r"S\d+E\d+").unwrap();
    let mut name = item.to_str().unwrap();
    if !opt.keep_square_brackets {
        let re_brackets = Regex::new(r"^\[[^\]]*\]").unwrap();
        if let Some(m) = re_brackets.find(name) {
            name = &name[m.end()..];
        }
    }
    let rename = match re_data.find(name) {
        Some(data) => {
            let nums: Vec<&str> = re_digits
                .find_iter(data.as_str())
                .map(|i| i.as_str())
                .collect();
            let episode = nums[1].parse::<i16>().unwrap();
            Some(file_rename(item, name, episode, &data, opt))
        }
        None => {
            let mut match_digits = re_digits.find_iter(name);
            for _ in 0..opt.offset {
                match_digits.next();
            }
            match match_digits.next() {
                Some(m) => {
                    let episode = m.as_str().parse::<i16>().unwrap();
                    Some(file_rename(item, name, episode, &m, opt))
                }
                None => {
                    println!("{}", format!("No episode number found in {}", name).red());
                    None
                }
            }
        }
    };
    rename
}

fn file_rename<'a>(
    original: &'a Path,
    name: &str,
    episode: i16,
    match_res: &Match,
    opt: &Opt,
) -> ToRename<'a> {
    let name_start = match &opt.name {
        Some(n) => n,
        None => &name[..match_res.start()],
    };
    let extension = match original.extension() {
        Some(e) => format!(".{}", e.to_str().unwrap()),
        None => String::from(""),
    };
    let new_name = match opt.season {
        Some(season) => format!(
            "{} S{:0>2}E{:0>2}{}",
            name_start.trim(),
            season,
            episode,
            extension
        ),
        None => format!("{} {:0>2}{}", name_start.trim(), episode, extension),
    };
    ToRename {
        original,
        new: new_name,
    }
}

#[cfg(test)]
mod test {
    use std::path::PathBuf;

    use crate::{gen_rename, Opt};

    #[test]
    fn test_bracket_end() {
        let item = PathBuf::from("[seeder] name - 01 [1080p].mkv");
        let opt = Opt {
            season: None,
            name: None,
            offset: 0,
            keep_square_brackets: false,
            yes_to_rename_prompt: false,
            files: Vec::new(),
        };
        let rename = gen_rename(&item, &opt);
        assert!(rename.is_some());
        assert_eq!(rename.unwrap().new, "name - 01.mkv");
    }

    #[test]
    fn test_offset() {
        let item = PathBuf::from("name s02 - 01 [1080p].mkv");
        let opt = Opt {
            season: None,
            name: Some(String::from("name")),
            offset: 1,
            keep_square_brackets: false,
            yes_to_rename_prompt: false,
            files: Vec::new(),
        };
        let rename = gen_rename(&item, &opt);
        assert!(rename.is_some());
        assert_eq!(rename.unwrap().new, "name 01.mkv");
    }

    #[test]
    fn test_namechange() {
        let item = PathBuf::from("[seeder] name - 01 [1080p].mkv");
        let opt = Opt {
            season: None,
            name: Some(String::from("new name")),
            offset: 0,
            keep_square_brackets: false,
            yes_to_rename_prompt: false,
            files: Vec::new(),
        };
        let rename = gen_rename(&item, &opt);
        assert!(rename.is_some());
        assert_eq!(rename.unwrap().new, "new name 01.mkv");
    }
}
